package com.example.forecastmvvm.data

import com.example.forecastmvvm.data.response.CurrentWeatherResponse
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

const val API_KEY = "b167d9667347623d5e3ba1fa0fe5ab25"
//full url http://api.weatherstack.com/current?access_key=b167d9667347623d5e3ba1fa0fe5ab25&query=New%20York
interface ApixuWeatherApiService {

    @GET("current")
    fun getCurrentWeather(
        @Query("query") location:String//,
        //@Query("lang") languageCode:String = "en"
    ): Deferred<CurrentWeatherResponse>

    companion object{
        operator fun invoke(): ApixuWeatherApiService{
            val requestInterceptor = Interceptor{chain ->

                val url = chain.request()                       // save as url
                    .url()
                    .newBuilder()
                    .addQueryParameter("access_key", API_KEY)  //add api key to query
                    .build()

                val request = chain.request()                   //pass url with apikey to request
                    .newBuilder()
                    .url(url)
                    .build()

                return@Interceptor chain.proceed(request)       // return request
            }

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(requestInterceptor)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("http://api.weatherstack.com/")
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create()) //converts json to kotlin object
                .build()
                .create(ApixuWeatherApiService::class.java)
        }
    }

}